var express = require('express');
var router = express.Router(),
    jwt = require('jsonwebtoken'),
    express = require('express'),
    app = express(),
    user = require('../dao/users.js'),
    manuscript = require('../dao/manuscripts.js'),

    config = require('../config/config'); // get our config file
    app.set('superSecret', config.secret); // secret variable


/*
 * Routes without authentication
 */


router.put('/registerUser/:email/:name:/:address', user.registerUser);




// ---------------------------------------------------------
// route middleware to authenticate and check token
// --------------------------------------------------------- 
router.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.param('token') || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, app.get('superSecret'), function (err, decoded) {
            if (err) {
                return res.json({success: false, message: 'Failed to authenticate token.'});
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }

});

// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------
//users

 router.get('/getUserInfo/:email', user.getUserInfo);//Це завданя для тебе! Пошук інформації про автора за його мейлом 
 

 //manuscripts
 router.post('/addNewManuscript', manuscript.addNewManuscript);
 router.get('/getManuscriptWithHigestPageQty', manuscript.getManuscriptsWithHigestPageQty); //Це завданя для тебе! Робота в якої найбільша кількість сторінок 
 


 
 
module.exports = router;


