var User = require('../models/user'),
    jwt  = require('jsonwebtoken'),
    express = require('express'),
    app  = express(),
    mongoose = require('mongoose'),
    config = require('../config/config');

  app.set('superSecret', config.secret);



var users = {


     registerUser: function(req, res) {
          var param = req.params;

    User.findOne({email: param.email }, function (err,user) {
      if (err) {
         res.status(500);
          res.json({
            "success": false,
            "message": "Internal Server Error."
          });
        console.log(err);
      } else {
        if (user) {
          res.status(409);
          res.json({
            "success": false,
            "message": "User already exists."
          });
        } 
        
        else {
    
      var newUser = new User({
                email: param.email, 
                name: param.name, 
                address: param.address
            });

        var token = jwt.sign(newUser, app.get('superSecret'), {
          expiresInMinutes: 525600 // expires in 1 year
        });
           
           var resultJson = {
                email: param.email, 
                name: param.name, 
                address: param.address,
                token: token


           };


          newUser.save(function(err,newUser) {
            if (err) {
                 res.status(500);
          res.json({
            "success": false,
            "message": "Internal Server Error."
          });
              console.error(err);
            } else {
                    
                res.status(200);
                res.json(resultJson);
            }
          });
    
   
            
        
        }


      }
    });
      
  },
  getUserInfo: function(req, res) {



  }

  
};

module.exports = users;