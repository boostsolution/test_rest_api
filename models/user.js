var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('users', new Schema({

    email: {type: String},
    name: {type: String},
    address: {type: String}
  


}));