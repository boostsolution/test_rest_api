var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('manuscripts', new Schema({

    title: {type: String},
    pageQty: {type: Number},
    chaptersQty: {type: Number},
    content: {type: String},
    author: {type: String}


}));