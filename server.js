// =================================================================
// get the packages we need ========================================
// =================================================================
var express 	= require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var http = require('http');



var config = require('./config/config'); // get our config file

// =================================================================
// configuration ===================================================
// =================================================================
// used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database


// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));



app.all('/', function(req, res) {
	res.send('The API allow: GET,POST');
});


 app.use('/api/V1', require('./routes'));
  // If no route is matched, return a 404
  app.use(function(req, res, next) {
   res.status(404);
	if (req.accepts('json')) {
    res.send({ error: 'Route not found' });
    return;
  }
  });

// =================================================================
// start the server ================================================
// =================================================================
var httpServer = http.createServer(app);

httpServer.listen(80);
console.log('Server started');
